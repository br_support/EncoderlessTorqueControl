
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF; 

void _INIT ProgramInit(void)
{
	AxisCtrl.Param.Acceleration = 200;
	AxisCtrl.Param.Velocity = 400;
	AxisCtrl.Param.Torque = 5;
	AxisCtrl.Param.TorqueRamp = 2;
	
	TorqueVelConst = 1.1;
	
	AxisCtrl.Param.Mode = VELOCITY_MODE;
	MC_BR_BrakeOperation_0.BrakeCommand = mcOPEN;

}

void _CYCLIC ProgramCyclic(void)
{
	/* Declare axis reference */
	pAxis = (UDINT)&gAxis01;
	
	MC_Power_0.Axis = pAxis;
	MC_Home_0.Axis = pAxis;
	MC_Reset_0.Axis = pAxis;
	MC_Stop_0.Axis = pAxis;
	
	MC_MoveVelocity_0.Axis = pAxis;
	MC_BR_BrakeOperation_0.Axis = pAxis;
	MC_BR_VelocityControl_0.Axis = pAxis;
	
	MC_ReadStatus_0.Axis = pAxis;
	MC_ReadActualPosition_0.Axis = pAxis;
	MC_ReadActualVelocity_0.Axis = pAxis;
	MC_ReadActualTorque_0.Axis = pAxis;
	
	
	/*************************************************************************************************************************/
	/* Global commands */
	// Digital inputs
	if ((DIStart) && (!Edges.Pos_0)) {
		Edges.Pos_0 = 1;
		AxisCtrl.Cmd.Start = 1;
	} else {
		if (!DIStart) {
			Edges.Pos_0 = 0;
		} 	
	}
	
	if ((DIStop) && (!Edges.Pos_1)) {
		Edges.Pos_1 = 1;
		AxisCtrl.Cmd.Stop = 1;
	} else {
		if (!DIStop) {
			Edges.Pos_1 = 0;
		} 	
	}
	
	
	
	// Visu inputs
	if (VisuData.Start) {
		VisuData.Start = 0;
		AxisCtrl.Cmd.Start = 1;
	}
	
	if (VisuData.Stop) {
		VisuData.Stop = 0;
		AxisCtrl.Cmd.Stop = 1;
	}
	
		
	if (VisuData.ErrorReset) {
		VisuData.ErrorReset = 0;
		AxisCtrl.Cmd.ErrorReset = 1;
	}
	
	/*************************************************************************************************************************/
	/* Monitor values */
	if (MC_ReadActualPosition_0.Valid) {
		AxisCtrl.Mon.ActualPosition = MC_ReadActualPosition_0.Position;
	} else {
		AxisCtrl.Mon.ActualPosition = -9999;
	}
	
	if (MC_ReadActualVelocity_0.Valid) {
		AxisCtrl.Mon.ActualVelocity = MC_ReadActualVelocity_0.Velocity;
	} else {
		AxisCtrl.Mon.ActualVelocity = -9999;
	}
	
	if (MC_ReadActualTorque_0.Valid) {
		AxisCtrl.Mon.ActualTorque = MC_ReadActualTorque_0.Torque;
	} else {
		AxisCtrl.Mon.ActualTorque = -9999;
	}
	
	
	
	
	if (MC_ReadStatus_0.Errorstop)	{
		AxisCtrl.Mon.State = AXIS_ERROR;
	} 
	
	if (MC_MoveVelocity_0.Error)   {
		AxisCtrl.Mon.State = AXIS_ERROR;
	}
	

	/*************************************************************************************************************************/
	/* Main state machine */
	switch (AxisCtrl.Mon.State)
	{
		case AXIS_DISABLED:
			
			AxisCtrl.Mon.Error = 0;
			
			MC_Power_0.Enable = 0;
			MC_MoveVelocity_0.Execute = 0;
			MC_Home_0.Execute = 0;
			MC_Stop_0.Execute = 0;
			MC_Reset_0.Execute = 0;
			AxisCtrl.Cmd.Stop = 0;
			AxisCtrl.Cmd.Start = 0;
			
			MC_ReadStatus_0.Enable = 1;
			MC_ReadActualPosition_0.Enable = 1;
			MC_ReadActualVelocity_0.Enable = 1;
			MC_ReadActualTorque_0.Enable = 1;
			
			if (MC_ReadStatus_0.Valid)  {
				AxisCtrl.Mon.State = AXIS_POWER_ON;
			}
			break;
	

		case AXIS_POWER_ON:
			
			MC_Power_0.Enable = 1;
			
			if (MC_Power_0.Status)  {
				AxisCtrl.Mon.State = AXIS_HOME;
			}
	       
			break;
		
		
		case AXIS_HOME:
			
			MC_Home_0.Execute = 1;
			MC_Home_0.HomingMode = 1;
			
			if (MC_Home_0.Done)  {
				AxisCtrl.Mon.State = AXIS_WAIT;
			}
	       
			break;
		
		
		case AXIS_WAIT:
			
			MC_MoveVelocity_0.Execute = 0;
			MC_Home_0.Execute = 0;
			MC_Stop_0.Execute = 0;
			MC_BR_VelocityControl_0.Enable = 0;
			
			if (MC_ReadStatus_0.StandStill) {
				if (AxisCtrl.Cmd.Start) {
					AxisCtrl.Cmd.Start = 0;
					if (AxisCtrl.Param.Mode == VELOCITY_MODE) {
						AxisCtrl.Mon.State = AXIS_MOVE_TORQUE;
					}
				}
			}

	       
			break;
		
		
		case AXIS_MOVE_TORQUE:	
       
			if (AxisCtrl.Cmd.Stop)  {
				AxisCtrl.Mon.State = AXIS_STOP;
				
				MC_BR_VelocityControl_0.Enable = 0;
			}
			
			
			MC_BR_VelocityControl_0.Enable = 1;
			MC_BR_VelocityControl_0.CyclicVelocity = AxisCtrl.Param.Velocity;
//			MC_BR_VelocityControl_0.CyclicVelocityCorrection = ;
			MC_BR_VelocityControl_0.CyclicTorque = AxisCtrl.Param.Torque;
			MC_BR_VelocityControl_0.TorqueMode = mcFF;
			MC_BR_VelocityControl_0.Acceleration = AxisCtrl.Param.Acceleration;
			MC_BR_VelocityControl_0.Deceleration = AxisCtrl.Param.Acceleration;
			MC_BR_VelocityControl_0.SctrlKv = 0.25;
			MC_BR_VelocityControl_0.SctrlTn = 0;
//			MC_BR_VelocityControl_0.InitSctrl = ;
			



			
			break;
		
		
		case AXIS_STOP:
			
			MC_Stop_0.Execute = 1;
			MC_Stop_0.Deceleration = AxisCtrl.Param.Acceleration;
			
			
			if (MC_Stop_0.Done)  {
				MC_Stop_0.Execute = 0;
				AxisCtrl.Cmd.Stop = 0;	
				//				AxisCtrl.Mon.State = AXIS_WAIT;
				AxisCtrl.Mon.State = AXIS_HOME;
			}
	       
			break;
		
		
		case AXIS_ERROR:
			
			AxisCtrl.Mon.Error = 1;
			AxisCtrl.Cmd.Start = 0;
			AxisCtrl.Cmd.Stop = 0;
			
	       
			if (AxisCtrl.Cmd.ErrorReset)  {
				MC_MoveVelocity_0.Execute = 0;
			
				AxisCtrl.Cmd.ErrorReset = 0;
			
				MC_Reset_0.Execute = 1;
				if (MC_Reset_0.Done) {
					MC_Reset_0.Execute = 0;
					AxisCtrl.Mon.State = AXIS_DISABLED;
				}
				
			}
			break;
	}
	     
	
	/*************************************************************************************************************************/
	/* Data to visualization */
	if (AxisCtrl.Param.Mode == TORQUE_MODE) {
		brsmemcpy((UDINT)&VisuData.Mode,(UDINT)&"MOMENTOVY REZIM",30);
	} else if (AxisCtrl.Param.Mode == VELOCITY_MODE) {
		brsmemcpy((UDINT)&VisuData.Mode,(UDINT)&"RYCHLOSTNI REZIM",30);
	}
	
	
	if (AxisCtrl.Mon.State == AXIS_DISABLED) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"OSA VYPNUTA",40);
	} else if (AxisCtrl.Mon.State == AXIS_POWER_ON) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"ZAPINANI OSY",40);
	} else if (AxisCtrl.Mon.State == AXIS_POWER_ON) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"ZAPINANI OSY",40);
	} else if (AxisCtrl.Mon.State == AXIS_HOME) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"REFEROVANI OSY",40);
	} else if (AxisCtrl.Mon.State == AXIS_WAIT) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"CEKA NA PRIKAZY",40);
	} else if (AxisCtrl.Mon.State == AXIS_MOVE_VELOCITY) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"BEZI NA RYCHLOST",40);
	} else if (AxisCtrl.Mon.State == AXIS_MOVE_TORQUE) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"BEZI NA MOMENT",40);
	} else if (AxisCtrl.Mon.State == AXIS_STOP) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"ZASTAVOVANI",40);
	} else if (AxisCtrl.Mon.State == AXIS_ERROR) {
		brsmemcpy((UDINT)&VisuData.State,(UDINT)&"OSA V CHYBE",40);
	}
		
		
	if (VisuData.SetVel) {
		VisuData.SetVel = 0;
		AxisCtrl.Param.Mode = VELOCITY_MODE;
	} else if (VisuData.SetTorque) {
		VisuData.SetTorque = 0;
		AxisCtrl.Param.Mode = TORQUE_MODE;
	}
	
	/*************************************************************************************************************************/
	/* Call function blocks */
	MC_Power(&MC_Power_0);
	MC_Home(&MC_Home_0);
	MC_Reset(&MC_Reset_0);
	MC_Stop(&MC_Stop_0);
	
	MC_MoveVelocity(&MC_MoveVelocity_0);
	MC_BR_BrakeOperation(&MC_BR_BrakeOperation_0);
	MC_BR_VelocityControl(&MC_BR_VelocityControl_0);
	
	MC_ReadStatus(&MC_ReadStatus_0);
	MC_ReadActualPosition(&MC_ReadActualPosition_0);
	MC_ReadActualVelocity(&MC_ReadActualVelocity_0);
	MC_ReadActualTorque(&MC_ReadActualTorque_0);

}

void _EXIT ProgramExit(void)
{
	MC_Power_0.Enable = 0;
	MC_Home_0.Execute = 0;
	MC_Reset_0.Execute = 0;
	MC_Stop_0.Execute = 0;
	
	MC_MoveVelocity_0.Execute = 0;
	
	MC_ReadStatus_0.Enable = 0;
	MC_ReadActualPosition_0.Enable = 0;
	MC_ReadActualVelocity_0.Enable = 0;
	MC_ReadActualTorque_0.Enable = 0;
	
	
	/* Call function blocks */
	MC_Power(&MC_Power_0);
	MC_Home(&MC_Home_0);
	MC_Reset(&MC_Reset_0);
	MC_Stop(&MC_Stop_0);
	
	MC_MoveVelocity(&MC_MoveVelocity_0);
	
	MC_ReadStatus(&MC_ReadStatus_0);
	MC_ReadActualPosition(&MC_ReadActualPosition_0);
	MC_ReadActualVelocity(&MC_ReadActualVelocity_0);
	MC_ReadActualTorque(&MC_ReadActualTorque_0);

}
