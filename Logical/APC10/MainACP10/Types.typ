TYPE
	AxisCtrlType : 	STRUCT 
		Mon : AxisMonCtrlType;
		Param : AxisParamCtrlType;
		Cmd : AxisCmdCtrlType;
	END_STRUCT;
	AxisParamCtrlModeEnum : 
		(
		VELOCITY_MODE := 10,
		TORQUE_MODE := 20
	);
	AxisParamCtrlType : 	STRUCT 
		Velocity : REAL;
		TorqueRamp : REAL;
		Torque : REAL;
		Acceleration : REAL;
		Mode : AxisParamCtrlModeEnum;
		Direction : BOOL;
	END_STRUCT;
	AxisMonStateEnum : 
		(
		AXIS_DISABLED := 0,
		AXIS_POWER_ON := 10,
		AXIS_HOME := 20,
		AXIS_WAIT := 30,
		AXIS_MOVE_VELOCITY := 40,
		AXIS_MOVE_TORQUE_START := 45,
		AXIS_MOVE_TORQUE := 50,
		AXIS_STOP := 60,
		AXIS_ERROR := 399
	);
	AxisMonCtrlType : 	STRUCT 
		Error : BOOL;
		ActualTorque : REAL;
		ActualVelocity : REAL;
		ActualPosition : REAL;
		State : AxisMonStateEnum;
	END_STRUCT;
	AxisCmdCtrlType : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		ErrorReset : BOOL;
	END_STRUCT;
	VisuDataType : 	STRUCT 
		Mode : STRING[30];
		SetTorque : BOOL;
		SetVel : BOOL;
		Start : BOOL;
		ErrorReset : USINT;
		Stop : BOOL;
		State : STRING[40];
	END_STRUCT;
	EdgesType : 	STRUCT 
		Pos_1 : BOOL;
		Pos_0 : BOOL;
	END_STRUCT;
END_TYPE
